from test_helper import run_common_tests, failed, passed, get_answer_placeholders


def test_ASCII(windows):
    for window in windows:
        all_ascii = all(ord(c) < 128 for c in window)
        if not all_ascii:
            failed("Please use only English characters this time.")
            return
    passed()


def test_Print(windows):
    if windows[0] == "print":
        passed()
    else:
        failed("Use Hint instructions to complete this task")


def test_is_alpha(window):
    is_multiline = window.find("\n")
    if is_multiline != -1:
        window = window[:is_multiline - 1]
    splitted = window.split()
    for s in splitted:
        if not s.isalpha():
            failed("Please use only English characters this time.")
            return

    passed()


if __name__ == '__main__':
    run_common_tests("You should enter your name")
    windows = get_answer_placeholders()
    test_ASCII(windows)
    test_is_alpha(windows[0])
    test_Print(windows)
