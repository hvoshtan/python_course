from test_helper import run_common_tests, failed, passed, get_answer_placeholders


def test_string_placeholders(placeholder):
    if placeholder != "string":  # TODO: your condition here
        passed()
    else:
        failed("Nope, sorry")


def test_variable_placeholder(placeholder):
    if placeholder == "a" or placeholder == "c":  # TODO: your condition here
        failed("Whoops, close one!")
    elif placeholder == "b":
        passed()
    else:
        failed("Select a, b or c!")


def test_comparsion_placeholder(placeholder):
    if placeholder == ">=" \
            or placeholder == ">" \
            or placeholder == "==":  # TODO: your condition here
        passed()
    elif placeholder == "<=" \
            or placeholder == "<" \
            or placeholder == "!=":
        failed("Almost done")
    else:
        failed("Use comparison operators!")


if __name__ == '__main__':
    placeholders = get_answer_placeholders()
    run_common_tests()
    test_string_placeholders(placeholders[0])
    test_variable_placeholder(placeholders[1])
    test_comparsion_placeholder(placeholders[2])
