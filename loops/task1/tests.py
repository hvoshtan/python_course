from test_helper import failed, passed, get_answer_placeholders


def test_While(windows):
    if windows == "while":
        passed()
    else:
        failed("Use Hint instructions to complete this task")


def test_Range(windows):
    if windows == "range":
        passed()
    else:
        failed("Use Hint instructions to complete this task")


if __name__ == '__main__':
    windows = get_answer_placeholders()
    test_While(windows[0])
    test_Range(windows[1])
